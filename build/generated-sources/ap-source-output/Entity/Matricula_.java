package Entity;

import Entity.Alumno;
import Entity.Curso;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-01-06T17:00:03")
@StaticMetamodel(Matricula.class)
public class Matricula_ { 

    public static volatile SingularAttribute<Matricula, Alumno> idAlumno;
    public static volatile SingularAttribute<Matricula, Character> modPago;
    public static volatile SingularAttribute<Matricula, Integer> idMatricula;
    public static volatile SingularAttribute<Matricula, Integer> idCuota;
    public static volatile SingularAttribute<Matricula, Curso> idCurso;
    public static volatile SingularAttribute<Matricula, Date> fchMatricula;
    public static volatile SingularAttribute<Matricula, String> beca;

}