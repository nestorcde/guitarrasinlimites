package Entity;

import Entity.Articulo;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-01-06T17:00:03")
@StaticMetamodel(TipoArticulo.class)
public class TipoArticulo_ { 

    public static volatile ListAttribute<TipoArticulo, Articulo> articuloList;
    public static volatile SingularAttribute<TipoArticulo, String> desTipoArticulo;
    public static volatile SingularAttribute<TipoArticulo, Integer> idTipoArticulo;

}