package Entity;

import Entity.Matricula;
import Entity.Profesor;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-01-06T17:00:03")
@StaticMetamodel(Curso.class)
public class Curso_ { 

    public static volatile SingularAttribute<Curso, Integer> valParcelado;
    public static volatile SingularAttribute<Curso, Date> fchFin;
    public static volatile SingularAttribute<Curso, Date> fchInicio;
    public static volatile SingularAttribute<Curso, Integer> valMatricula;
    public static volatile SingularAttribute<Curso, Profesor> idProfesor;
    public static volatile ListAttribute<Curso, Matricula> matriculaList;
    public static volatile SingularAttribute<Curso, Integer> idCurso;
    public static volatile SingularAttribute<Curso, Integer> duracion;
    public static volatile SingularAttribute<Curso, String> nomCurso;
    public static volatile SingularAttribute<Curso, Integer> valContado;

}