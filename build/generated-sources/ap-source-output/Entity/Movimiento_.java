package Entity;

import Entity.Articulo;
import Entity.Cuota;
import Entity.Matricula;
import Entity.MovimientoPK;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-01-06T17:00:03")
@StaticMetamodel(Movimiento.class)
public class Movimiento_ { 

    public static volatile SingularAttribute<Movimiento, Articulo> idArticulo;
    public static volatile SingularAttribute<Movimiento, Cuota> cuota;
    public static volatile SingularAttribute<Movimiento, MovimientoPK> movimientoPK;
    public static volatile SingularAttribute<Movimiento, Matricula> idMatricula;
    public static volatile SingularAttribute<Movimiento, Date> fchMov;
    public static volatile SingularAttribute<Movimiento, Integer> impMov;
    public static volatile SingularAttribute<Movimiento, Integer> cantArticulo;
    public static volatile SingularAttribute<Movimiento, Integer> numRecibo;

}