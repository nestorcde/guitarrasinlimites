package Entity;

import Entity.CuotaPK;
import Entity.Movimiento;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-01-06T17:00:03")
@StaticMetamodel(Cuota.class)
public class Cuota_ { 

    public static volatile SingularAttribute<Cuota, Integer> impCuota;
    public static volatile SingularAttribute<Cuota, CuotaPK> cuotaPK;
    public static volatile SingularAttribute<Cuota, Character> estCuota;
    public static volatile ListAttribute<Cuota, Movimiento> movimientoList;
    public static volatile SingularAttribute<Cuota, Date> fchVencimiento;
    public static volatile SingularAttribute<Cuota, Date> fchPago;

}