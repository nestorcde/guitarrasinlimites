package Entity;

import Entity.Movimiento;
import Entity.TipoArticulo;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-01-06T17:00:03")
@StaticMetamodel(Articulo.class)
public class Articulo_ { 

    public static volatile SingularAttribute<Articulo, Integer> idArticulo;
    public static volatile SingularAttribute<Articulo, Integer> existencia;
    public static volatile SingularAttribute<Articulo, String> desArticulo;
    public static volatile SingularAttribute<Articulo, Integer> precioCompra;
    public static volatile ListAttribute<Articulo, Movimiento> movimientoList;
    public static volatile SingularAttribute<Articulo, Integer> precioVenta;
    public static volatile SingularAttribute<Articulo, TipoArticulo> idTipoArticulo;

}