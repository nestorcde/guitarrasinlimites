package Entity;

import Entity.Alumno;
import Entity.Ciudad;
import Entity.Profesor;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-01-06T17:00:03")
@StaticMetamodel(Persona.class)
public class Persona_ { 

    public static volatile SingularAttribute<Persona, String> nomPersona;
    public static volatile SingularAttribute<Persona, String> nroCedula;
    public static volatile SingularAttribute<Persona, Alumno> alumno;
    public static volatile SingularAttribute<Persona, byte[]> fotoPersona;
    public static volatile ListAttribute<Persona, Profesor> profesorList;
    public static volatile SingularAttribute<Persona, Ciudad> idCiudad;
    public static volatile SingularAttribute<Persona, String> dirPersona;
    public static volatile SingularAttribute<Persona, String> apePersona;
    public static volatile SingularAttribute<Persona, Character> sexo;
    public static volatile SingularAttribute<Persona, Date> fchNacimiento;
    public static volatile SingularAttribute<Persona, Integer> idPersona;
    public static volatile SingularAttribute<Persona, String> email;
    public static volatile SingularAttribute<Persona, String> telPersona;

}