package Entity;

import Entity.Persona;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-01-06T17:00:03")
@StaticMetamodel(Ciudad.class)
public class Ciudad_ { 

    public static volatile SingularAttribute<Ciudad, String> nomCiudad;
    public static volatile ListAttribute<Ciudad, Persona> personaList;
    public static volatile SingularAttribute<Ciudad, Integer> idCiudad;

}