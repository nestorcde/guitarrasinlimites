package vista.Articulo;


import Entity.Articulo;
import Entity.TipoArticulo;
import control.ArticuloControl;
import control.TipoArticuloControl;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

public class ArticulosDlg extends javax.swing.JFrame {
    ArticuloControl articuloControl = new ArticuloControl();
    TipoArticuloControl tipoArticuloControl = new TipoArticuloControl();
    Articulo articulo = Articulo.newInstance();
    TipoArticulo tipoArticulo = TipoArticulo.newInstance();
    List<TipoArticulo> tipoArticuloList;
    List<Articulo> articulosList;
    
    
    
    public ArticulosDlg() {
        initComponents();
        this.setLocationRelativeTo(null);
        
        tipoArticuloList = tipoArticuloControl.getTipoArticulos();
        for (int c = 0; c<tipoArticuloList.size(); c++){
            tipoArticulo = (TipoArticulo) tipoArticuloList.get(c);
            cbxCategoria.addItem(tipoArticulo.getDesTipoArticulo());
        }
        
        refresh();
    }
    
    private void desbloquear() {
        btnEliminar.setEnabled(true);
        btnModificar.setEnabled(true);
    }
    
    private void bloquear() {
        btnEliminar.setEnabled(false);
        btnModificar.setEnabled(false);
    }
    
    public void refresh(){
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("ID");
        modelo.addColumn("Descripcion");
        modelo.addColumn("Categoria");
        modelo.addColumn("P. Compra");
        modelo.addColumn("P. Venta");
        modelo.addColumn("Stock");
        
        articulosList = articuloControl.getArticulos();
        Object[] fila = new Object[6];
        for (int i = 0; i < articulosList.size(); i++) {
            articulo = (Articulo) articulosList.get(i);
            fila[0] = articulo.getIdArticulo();
            fila[1] = articulo.getDesArticulo().trim();
            fila[2] = articulo.getIdTipoArticulo().getDesTipoArticulo();
            fila[3] = articulo.getPrecioCompra();
            fila[4] = articulo.getPrecioVenta();
            fila[5] = articulo.getExistencia();
            modelo.addRow(fila);
        }
        
        tblProductos.setModel(modelo);
        
        Integer ancho = tblProductos.getSize().width;
            
            TableColumnModel columnModel = tblProductos.getColumnModel();
            columnModel.getColumn(0).setPreferredWidth(ancho*5/100);
            columnModel.getColumn(1).setPreferredWidth(ancho*30/100);
            columnModel.getColumn(2).setPreferredWidth(ancho*30/100);
            columnModel.getColumn(3).setPreferredWidth(ancho*13/100);
            columnModel.getColumn(4).setPreferredWidth(ancho*13/100);
            columnModel.getColumn(5).setPreferredWidth(ancho*9/100);
        
        txtPrecioVta.setText("");
        txtPrecioCmp.setText("");
        txtNombre.setText("");
        txtNombre.requestFocus();
        txtStock.setText("");
        cbxCategoria.setSelectedIndex(0);

        bloquear();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblProductos = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        txtPrecioVta = new javax.swing.JTextField();
        btnCerrar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txtPrecioCmp = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        btnLimpiar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtStock = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        cbxCategoria = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Productos");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Lista de Productos"));

        tblProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tblProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblProductosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblProductos);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 685, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 385, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Registrar Producto"));

        txtPrecioVta.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnGuardar.setText("Guardar Nuevo");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        jLabel4.setText("Precio Compra");

        jLabel5.setText("Precio Venta");

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        jLabel1.setText("Stock");

        txtStock.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel2.setText("Categoria");

        jLabel6.setText("Nombre");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCerrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(cbxCategoria, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtPrecioVta, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnEliminar)
                            .addComponent(btnGuardar)
                            .addComponent(btnModificar)))
                    .addComponent(jLabel2)
                    .addComponent(jLabel5)
                    .addComponent(jLabel1)
                    .addComponent(txtStock, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombre)
                    .addComponent(jLabel4)
                    .addComponent(txtPrecioCmp)
                    .addComponent(jLabel6))
                .addContainerGap())
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnEliminar, btnGuardar, btnModificar});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtPrecioCmp, txtPrecioVta});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPrecioCmp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPrecioVta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbxCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(btnGuardar)
                .addGap(5, 5, 5)
                .addComponent(btnModificar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEliminar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCerrar)
                    .addComponent(btnLimpiar))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        Integer id = 0;
        String nombre = txtNombre.getText();
        Integer precioCmp = Integer.parseInt(txtPrecioCmp.getText());
        Integer precioVta = Integer.parseInt(txtPrecioVta.getText());
        Integer stock = Integer.parseInt(txtStock.getText());
        System.out.println(" Tipo Articulo "+cbxCategoria.getSelectedItem().toString().trim().toUpperCase());
        tipoArticuloList = tipoArticuloControl.getTipoArticulos(cbxCategoria.getSelectedItem().toString().trim().toUpperCase());
        
        tipoArticulo = tipoArticuloList.get(0);
        if(!nombre.equals("")&&precioCmp!=null&&precioVta!=null&&stock!=null&&tipoArticulo!=null){
            articulo.setIdArticulo(id);
            articulo.setDesArticulo(nombre);
            articulo.setPrecioCompra(precioCmp);
            articulo.setPrecioVenta(precioVta);
            articulo.setExistencia(stock);
            articulo.setIdTipoArticulo(tipoArticulo);
            articuloControl.grabar(articulo);
            refresh();
        }

        
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void tblProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblProductosMouseClicked
        Integer ide = (Integer) tblProductos.getValueAt(tblProductos.getSelectedRow(), 0);
        if(ide>0){
            articulosList = articuloControl.getArticulos(ide);
            articulo = articulosList.get(0);
        }
        
        if (tblProductos.getSelectedRow() >= 0){
            desbloquear();
            
            txtNombre.setText(articulo.getDesArticulo());
            txtPrecioCmp.setText(String.valueOf((articulo.getPrecioCompra())));
            txtPrecioVta.setText(String.valueOf((articulo.getPrecioVenta())));
            txtStock.setText(String.valueOf((articulo.getExistencia())));
            cbxCategoria.setSelectedIndex(articulo.getIdTipoArticulo().getIdTipoArticulo()-1);
        }else{
            bloquear();
        }
    }//GEN-LAST:event_tblProductosMouseClicked

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        refresh();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        
        Integer filaNum = tblProductos.getSelectedRow();
        if( filaNum<0 ){
            JOptionPane.showMessageDialog(null, "Debe seleccionar un registro");
        } else {
            int dialogResult = JOptionPane.showConfirmDialog(null, "¿Está seguro de querer borrar el registro?", "Eliminar", JOptionPane.OK_CANCEL_OPTION);
            if (dialogResult == JOptionPane.YES_OPTION){
            Integer id = (Integer) tblProductos.getValueAt(filaNum, 0);
            articulo = articuloControl.getArticulos(id).get(0);
            articuloControl.eliminar(articulo);
            this.refresh();
            }
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        
        Integer id = (Integer) tblProductos.getValueAt(tblProductos.getSelectedRow(), 0);
        if(id>0){
            int dialogResult = JOptionPane.showConfirmDialog(null, "¿Está seguro de querer modificar el registro?", "Modificar", JOptionPane.OK_CANCEL_OPTION);
            if (dialogResult == JOptionPane.YES_OPTION){
                String nombre = txtNombre.getText();
                Integer precioCmp = Integer.parseInt(txtPrecioCmp.getText());
                Integer precioVta = Integer.parseInt(txtPrecioVta.getText());
                Integer stock = Integer.parseInt(txtStock.getText());
                tipoArticuloList = tipoArticuloControl.getTipoArticulos(cbxCategoria.getSelectedItem().toString().trim());
                TipoArticulo tipoArticulo = tipoArticuloList.get(0);
                if(!nombre.equals("")&&precioVta!=null&&stock!=null&&tipoArticulo!=null){
                    articulo.setIdArticulo(id);
                    articulo.setDesArticulo(nombre);
                    articulo.setPrecioCompra(precioCmp);
                    articulo.setPrecioVenta(precioVta);
                    articulo.setExistencia(stock);
                    articulo.setIdTipoArticulo(tipoArticulo);
                    articuloControl.grabar(articulo);
                    refresh();
                }
            }
        }
        
    }//GEN-LAST:event_btnModificarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ArticulosDlg.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ArticulosDlg.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ArticulosDlg.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ArticulosDlg.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ArticulosDlg().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JComboBox<String> cbxCategoria;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblProductos;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtPrecioCmp;
    private javax.swing.JTextField txtPrecioVta;
    private javax.swing.JTextField txtStock;
    // End of variables declaration//GEN-END:variables
}
