/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.Matricula;

import Entity.Alumno;
import Entity.Cuota;
import Entity.CuotaPK;
import Entity.Curso;
import Entity.Matricula;
import control.CuotaControl;
import control.MatriculaControl;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import javax.swing.table.DefaultTableModel;
import vista.Alumno.SelAlumnoFrm;
import vista.Curso.SelCursoFrm;

/**
 *
 * @author nestor
 */
public class MatriculaFrm extends javax.swing.JDialog {

    /**
     * Creates new form MatriculaFrm
     */
    
    CuotaControl cuotaControl = new CuotaControl();
    List<Cuota> cuotaList;
    Cuota cuota = new Cuota();
    public Curso curso;
    public Alumno alumno;
    Integer valCuota;
    Integer priCuota;
    MatriculaControl matriculaControl = new MatriculaControl();
    ListaMatriculasFrm listaMatriculasFrm;
    Integer idMatricula = 0;
    Matricula matricula = new Matricula();
    String mod;
    
    public MatriculaFrm() {
        initComponents();
        this.setLocationRelativeTo(null);
        cargarCombos();
        //cargarTabla();
    }
    
    public MatriculaFrm(ListaMatriculasFrm aThis, String mod, Integer id, Boolean modal) {
        initComponents();
        this.setLocationRelativeTo(aThis);
        listaMatriculasFrm = aThis;
        cargarCombos();
        this.mod = mod;
        this.setModal(true);
        if(mod.equals("MOD")){
            idMatricula = id;
            matricula = matriculaControl.getMatriculas(id).get(0);
            this.curso = matricula.getIdCurso();
            this.alumno = matricula.getIdAlumno();
            lblCodigo.setText(matricula.getIdMatricula().toString());
            txtAlumno.setText(matricula.getIdAlumno().getIdPersona().getNomPersona().trim()+" "+matricula.getIdAlumno().getIdPersona().getApePersona().trim());
            txtCurso.setText(matricula.getIdCurso().getNomCurso().trim());
            txtFchFin.setText(retFecha(matricula.getIdCurso().getFchFin()));
            txtFchInicio.setText(retFecha(matricula.getFchMatricula()));
            switch(matricula.getBeca()){
                case "SB":
                    cmbBeca.setSelectedIndex(0);
                    break;
                case "MB":
                    cmbBeca.setSelectedIndex(1);
                    break;
                case "BC":
                    cmbBeca.setSelectedIndex(2);
                    break;
            }
            cmbMetodo.setSelectedIndex(0);
            if(matricula.getModPago()=='P'){
                cmbMetodo.setSelectedIndex(1);
                cargarTabla(idMatricula);
            }
            
            //Para evitar que se modifique despues del inicio del Curso
            try{
                Calendar fchIni = Calendar.getInstance();
                Calendar fchHoy = Calendar.getInstance(TimeZone.getTimeZone("GMT -04:00"));
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date datHoy = sdf.parse(retFecha(fchHoy.getTime()));
                Date datIni = sdf.parse(retFecha(matricula.getFchMatricula()));
                datHoy.getTime();

                fchIni.setTime(matricula.getFchMatricula());
                //JOptionPane.showMessageDialog(null,"datHoy = "+sdf.format(datHoy)+" matricula.getFchMatricula() = "+sdf.format(matricula.getFchMatricula().toString()));
                if(fchHoy.after(fchIni) && datHoy.compareTo(datIni)!=0){
                    btnSelAlumno.setEnabled(false);
                    btnSelCurso.setEnabled(false);
                    cmbMetodo.setEnabled(false);
                    cmbBeca.setEnabled(false);
                }
            }catch(ParseException ex){
                System.out.println(ex.getMessage());
            }
            
            this.mod = mod;
        }
        if(mod.equals("VER")){
            idMatricula = id;
            matricula = matriculaControl.getMatriculas(id).get(0);
            lblCodigo.setText(matricula.getIdMatricula().toString());
            txtAlumno.setText(matricula.getIdAlumno().getIdPersona().getNomPersona().trim()+" "+matricula.getIdAlumno().getIdPersona().getApePersona().trim());
            txtCurso.setText(matricula.getIdCurso().getNomCurso().trim());
            txtFchFin.setText(retFecha(matricula.getIdCurso().getFchFin()));
            txtFchInicio.setText(retFecha(matricula.getFchMatricula()));
            switch(matricula.getBeca()){
                case "SB":
                    cmbBeca.setSelectedIndex(0);
                    break;
                case "MB":
                    cmbBeca.setSelectedIndex(1);
                    break;
                case "BC":
                    cmbBeca.setSelectedIndex(2);
                    break;
            }
            cmbMetodo.setSelectedIndex(0);
            if(matricula.getModPago()=='P'){
                cmbMetodo.setSelectedIndex(1);
                cargarTabla(idMatricula);
            }
            btnGuardar.setText("SALIR");
            cmbMetodo.setEnabled(false);
            cmbBeca.setEnabled(false);
            btnSelAlumno.setEnabled(false);
            btnSelCurso.setEnabled(false);
            this.mod = mod;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCuota = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        lblCodigo = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtAlumno = new javax.swing.JTextField();
        btnSelAlumno = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txtCurso = new javax.swing.JTextField();
        btnSelCurso = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtFchInicio = new javax.swing.JTextField();
        txtFchFin = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        cmbMetodo = new javax.swing.JComboBox<>();
        btnGuardar = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        cmbBeca = new javax.swing.JComboBox<>();

        jLabel3.setText("jLabel3");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CUOTAS", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        tblCuota.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblCuota);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 649, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("CODIGO:");

        lblCodigo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("ALUMNO:");

        txtAlumno.setEditable(false);
        txtAlumno.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtAlumno.setEnabled(false);
        txtAlumno.setFocusable(false);

        btnSelAlumno.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnSelAlumno.setText("...");
        btnSelAlumno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelAlumnoActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("CURSO:");

        txtCurso.setEditable(false);
        txtCurso.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtCurso.setEnabled(false);
        txtCurso.setFocusable(false);

        btnSelCurso.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnSelCurso.setText("...");
        btnSelCurso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelCursoActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("MATRICULAS");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel6.setText("F. INICIO:");

        txtFchInicio.setEditable(false);
        txtFchInicio.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtFchInicio.setEnabled(false);
        txtFchInicio.setFocusable(false);

        txtFchFin.setEditable(false);
        txtFchFin.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtFchFin.setEnabled(false);
        txtFchFin.setFocusable(false);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel7.setText("F. FIN:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel8.setText("MODO DE PAGO:");

        cmbMetodo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        btnGuardar.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        btnGuardar.setText("GRABAR");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel9.setText("TIPO DE BECA:");

        cmbBeca.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbBeca, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbMetodo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCurso)
                            .addComponent(txtAlumno)
                            .addComponent(lblCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(txtFchInicio, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtFchFin, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnSelAlumno, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnSelCurso, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(24, 24, 24))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnGuardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cmbMetodo, txtFchFin});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCodigo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtAlumno, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSelAlumno, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtCurso, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSelCurso, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtFchInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtFchFin, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(cmbMetodo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(cmbBeca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cmbMetodo, txtFchFin});

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSelAlumnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelAlumnoActionPerformed
        SelAlumnoFrm alumnoFrm = new SelAlumnoFrm(this);
        alumnoFrm.setVisible(true);
    }//GEN-LAST:event_btnSelAlumnoActionPerformed

    private void btnSelCursoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelCursoActionPerformed
        SelCursoFrm cursoFrm = new SelCursoFrm(this);
        cursoFrm.setVisible(true);
    }//GEN-LAST:event_btnSelCursoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        if(!txtAlumno.getText().equals("") && !txtCurso.getText().equals("") && (mod.equals("INS")||mod.equals("MOD"))){
            try {
                grabar();
                listaMatriculasFrm.cargarTabla();
                dispose();
            } catch (ParseException ex) {
                System.out.println("Error al Parsear "+ex);
            }
        }else{
            dispose();
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MatriculaFrm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MatriculaFrm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MatriculaFrm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MatriculaFrm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MatriculaFrm().setVisible(true);
            }
        });
    }
    
    void cargarTabla(Integer idMatricula){
            tblCuota.removeAll();
            matriculaControl = new MatriculaControl();
            matricula = matriculaControl.getMatriculas(idMatricula).get(0);
            cuotaControl = new CuotaControl();
            cuotaList = cuotaControl.getCuotas(Integer.valueOf(matricula.getIdCuota().toString()));
            cuota = new Cuota();
            
            String[] titulos = {"ID","SEC","Importe","F.Venc.","Estado","F. Pago"};
            DefaultTableModel modelo = new DefaultTableModel(null,titulos) ;
            

            
            Object[] fila = new Object[6];

            for( int i=0; i<cuotaList.size(); i++ ){
                cuota = (Cuota) cuotaList.get(i);
               
                fila[0] = cuota.getCuotaPK().getIdCuota() ;
                fila[1] = cuota.getCuotaPK().getSecCuota();
                fila[2] = cuota.getImpCuota();
                
                
                fila[3] = retFecha(cuota.getFchVencimiento());
                String estado;
                if(cuota.getEstCuota() == 'P'){
                    estado = "Pendiente";
                }else{
                    estado = "Pagado";
                }
                fila[4] = estado;
                if(cuota.getFchPago()!=null){
                    fila[5] = retFecha(cuota.getFchPago());
                }else{
                    fila[5] = "";
                }
                 
                
                modelo.addRow(fila);
            }
            tblCuota.setModel(modelo);
//            TableColumnModel columnModel = tblCuota.getColumnModel();
//            columnModel.getColumn(0).setPreferredWidth(48);
//            columnModel.getColumn(1).setPreferredWidth(48);
//            columnModel.getColumn(2).setPreferredWidth(55);
            
            //limpiar();
    }
    
    public String retFecha(Date date){
        Calendar cal = Calendar.getInstance();
        String fecha = null;
        cal.setTime(date);
        //cal.add(Calendar.DATE, 1);
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
        if(cal!=null){
            fecha = formato.format(cal.getTime());
        }
        return fecha;
    }
    
    public void grabar() throws ParseException{
        Matricula matricula = new Matricula();
        Date fchIni;
        Integer desc = 0;
        
        
        if(mod.equals("INS")){
            matricula.setIdAlumno(alumno);
            matricula.setIdCurso(curso);
            String fch = txtFchInicio.getText();
            fchIni = new SimpleDateFormat("dd-MM-yyyy").parse(fch);
            matricula.setFchMatricula(fchIni);
        }else{
            //matricula.setIdMatricula(Integer.parseInt(lblCodigo.getText()));
            matricula = matriculaControl.getMatriculas(Integer.parseInt(lblCodigo.getText())).get(0);
        }
        if(cmbBeca.getSelectedItem().equals("BECA COMPLETA")){
            cmbMetodo.setSelectedIndex(0);
        }
        matricula.setModPago(cmbMetodo.getSelectedItem().toString().charAt(0));
        String beca = cmbBeca.getSelectedItem().toString();
        switch(beca){
            case "SIN BECA":
                matricula.setBeca("SB");
                desc = 0;
                break;
            case "MEDIA BECA":
                matricula.setBeca("MB");
                desc = 50;
                break;
            case "BECA COMPLETA":
                matricula.setBeca("BC");
                desc = 100;
                break;
        }
        if(cmbMetodo.getSelectedItem().toString().equals("PARCELADO")){
            SimpleDateFormat sdt = new SimpleDateFormat("dd-MM-yyyy");
            Calendar fchFinCurso = Calendar.getInstance();
            Calendar fchIniCurso = Calendar.getInstance();
            Calendar fchIniMat = Calendar.getInstance();
            String fch = txtFchInicio.getText();
            fchIni = new SimpleDateFormat("dd-MM-yyyy").parse(fch);
            fchIniMat.setTime(fchIni);
            fchIniCurso.setTime(curso.getFchInicio());
            fchFinCurso.setTime(curso.getFchFin());
            valCuota = curso.getValParcelado();
            valCuota = valCuota - (valCuota * desc / 100);
            Integer idCuota = cuotaControl.idCuota();
            Integer cantCuotas = getCantCuotas(fchFinCurso, fchIniMat);
            priCuota = valCuota;
            Integer mes = fchIniMat.get(Calendar.MONTH);
            Integer anho = fchIniMat.get(Calendar.YEAR);
            if(fchIniMat.after(fchIniCurso)){
                if(fchIniMat.get(Calendar.DAY_OF_MONTH)>10){
                    Integer dia = 30 - fchIniMat.get(Calendar.DAY_OF_MONTH);
                    priCuota = valCuota/30*dia;
                    //JOptionPane.showMessageDialog(null, dia+" - "+priCuota);
                    
                    
                }
            }
            for(int i = 1; i<=cantCuotas;i++){
                Calendar fchVencCal = Calendar.getInstance();
                Date fchVenc = new Date();
                if(i == 1){
                    cuota.setImpCuota(priCuota);
                }else{
                    cuota.setImpCuota(valCuota);
                }
                if(mes==12){
                    mes=1;
                    anho+=1;
                }else{
                    mes += 1;
                }
                fchVencCal.set(anho, mes, 10);
                fchVenc = fchVencCal.getTime();
                cuota.setFchVencimiento(fchVenc);
                cuota.setCuotaPK(new CuotaPK(idCuota, i));
                cuota.setEstCuota('P');
                cuotaControl.grabar(cuota, "INS");
            }
            matricula.setIdCuota(idCuota);
        }else{
            Matricula mat = new Matricula();
            if(mod.equals("MOD")){
                mat = matriculaControl.getMatriculas(Integer.parseInt(lblCodigo.getText())).get(0);
            }
            if(mat.getIdCuota()!=null){
                cuotaControl.eliminar(mat.getIdCuota());
            }
            matricula.setIdCuota(0);
            //matricula.setModPago(Character.MIN_VALUE);
        }
        
        matriculaControl.grabar(matricula);
        
    }
    
    public Integer getCantCuotas(Calendar fchFin,Calendar fchIni){
        Integer difA = fchFin.get(Calendar.YEAR) - fchIni.get(Calendar.YEAR);
            Integer cantCuotas = difA * 12 + fchFin.get(Calendar.MONTH) - fchIni.get(Calendar.MONTH);
            return cantCuotas;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnSelAlumno;
    private javax.swing.JButton btnSelCurso;
    private javax.swing.JComboBox<String> cmbBeca;
    private javax.swing.JComboBox<String> cmbMetodo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblCodigo;
    private javax.swing.JTable tblCuota;
    public javax.swing.JTextField txtAlumno;
    public javax.swing.JTextField txtCurso;
    public javax.swing.JTextField txtFchFin;
    public javax.swing.JTextField txtFchInicio;
    // End of variables declaration//GEN-END:variables

    private void cargarCombos() {
        cmbMetodo.addItem("CONTADO");
        cmbMetodo.addItem("PARCELADO");
        cmbBeca.addItem("SIN BECA");
        cmbBeca.addItem("MEDIA BECA");
        cmbBeca.addItem("BECA COMPLETA");
    }
}
