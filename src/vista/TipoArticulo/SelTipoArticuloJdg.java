/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.TipoArticulo;

import Entity.TipoArticulo;
import control.TipoArticuloControl;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
   
/**
 *
 * @author nestor
 */
public class SelTipoArticuloJdg extends javax.swing.JDialog {
    
    TipoArticuloControl tipoArticuloControl = new TipoArticuloControl();
    List<TipoArticulo> tipoArticuloList;
    TipoArticulo tipoArticulo = new TipoArticulo();
    String origen = null;

    /**
     * Creates new form SelTipoArticuloJdg
     */
    public SelTipoArticuloJdg(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        cargarTabla();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTipoArticulos = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtBuscar = new javax.swing.JTextField();
        btnSeleccionar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(754, 396));
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Seleccion de Tipo de Articulo", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        tblTipoArticulos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblTipoArticulos.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tblTipoArticulosFocusGained(evt);
            }
        });
        tblTipoArticulos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblTipoArticulosMouseReleased(evt);
            }
        });
        tblTipoArticulos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tblTipoArticulosKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tblTipoArticulosKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblTipoArticulos);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("BUSCAR");

        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 86, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        btnSeleccionar.setText("Seleccionar - F3");
        btnSeleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarActionPerformed(evt);
            }
        });
        btnSeleccionar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnSeleccionarKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSeleccionar, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnSeleccionar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tblTipoArticulosFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tblTipoArticulosFocusGained
        if(tblTipoArticulos.getSelectedRow()>=0){
            switch (origen){
                case "tipoArticulo":
                cargarModificar();
                break;
                case "profesor":
                //                    cargarModificarProfesor();
                break;
            }
        }
    }//GEN-LAST:event_tblTipoArticulosFocusGained

    private void tblTipoArticulosMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblTipoArticulosMouseReleased
        if(tblTipoArticulos.getSelectedRow()>=0){
            switch (origen){
                case "tipoArticulo":
                cargarModificar();
                break;
                case "profesor":
                //cargarModificarProfesor();
                break;
            }
        }
    }//GEN-LAST:event_tblTipoArticulosMouseReleased

    private void tblTipoArticulosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblTipoArticulosKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_F3){
            seleccionar();
        }
    }//GEN-LAST:event_tblTipoArticulosKeyPressed

    private void tblTipoArticulosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblTipoArticulosKeyReleased
        if(tblTipoArticulos.getSelectedRow()>=0){
            switch (origen){
                case "tipoArticulo":
                cargarModificar();
                break;
            }
        }
    }//GEN-LAST:event_tblTipoArticulosKeyReleased

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased
        if(txtBuscar.getText().length()>0 ){
            cargarTablaNombre(txtBuscar.getText().trim().toUpperCase());
        }else{
            cargarTabla();
        }
    }//GEN-LAST:event_txtBuscarKeyReleased

    private void btnSeleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionarActionPerformed
        seleccionar();
    }//GEN-LAST:event_btnSeleccionarActionPerformed

    private void btnSeleccionarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnSeleccionarKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_F3){
            seleccionar();
        }
    }//GEN-LAST:event_btnSeleccionarKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SelTipoArticuloJdg.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SelTipoArticuloJdg.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SelTipoArticuloJdg.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SelTipoArticuloJdg.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                SelTipoArticuloJdg dialog = new SelTipoArticuloJdg(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    
    void cargarTabla(){
            
            tblTipoArticulos.removeAll();
            tipoArticuloControl = new TipoArticuloControl();
            tipoArticuloList = tipoArticuloControl.getTipoArticulos();
            tipoArticulo = new TipoArticulo();
            String[] titulos = {"ID","Tipo de Articulo"};
            DefaultTableModel modelo = new DefaultTableModel(null,titulos) ;
            Object[] fila = new Object[2];

            for( int i=0; i<tipoArticuloList.size(); i++ ){
                tipoArticulo = (TipoArticulo) tipoArticuloList.get(i);
                //nac =  persona.getFchNacimiento();
                //edad = (int)((HOY.getTime() - nac.getTime())/86400000)/365;
                fila[0] = tipoArticulo.getIdTipoArticulo().toString().trim() ;
                fila[1] = tipoArticulo.getDesTipoArticulo().toString().trim();
                
                modelo.addRow(fila);
            }
            tblTipoArticulos.setModel(modelo);
            Integer ancho = tblTipoArticulos.getSize().width;
            
            TableColumnModel columnModel = tblTipoArticulos.getColumnModel();
            columnModel.getColumn(0).setPreferredWidth(ancho*5/100);
            columnModel.getColumn(1).setPreferredWidth(ancho*95/100);
            
            //limpiar();
    }
    
    
    
    void cargarModificar(){
        Integer numLinea = tblTipoArticulos.getSelectedRow();
        //ImageIcon foto;
        if(numLinea > -1){
            Integer id = Integer.parseInt(tblTipoArticulos.getValueAt(numLinea, 0).toString().trim());
            
            //JOptionPane.showMessageDialog(null, tblCiudades.getValueAt(numLinea, 0)+" "+tblCiudades.getValueAt(numLinea, 1));
        }else{
            JOptionPane.showMessageDialog(null, "Seleccione registro a modificar");
        }
    }
    
    void seleccionar(){
        switch (origen){
            case "alumno":
                //alumnoFrm.btnGuardar.requestFocus();
                break;
            case "profesor":
                //profesorFrm.btnGuardar.requestFocus();
                
                break;
        }
        
        dispose();
    }
    
    public String retFecha(Date date){
        Calendar cal = Calendar.getInstance();
        String fecha = null;
                cal.setTime(date);
                //cal.add(Calendar.DATE, 1);
                SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
                if(cal!=null){
                    fecha = formato.format(cal.getTime());
                }
        return fecha;
    }
    
    void cargarTablaNombre(String nombre){
            //JOptionPane.showMessageDialog(null, txtNombre.getText()+" "+txtCedula.getText());
            tblTipoArticulos.removeAll();
            tipoArticuloControl = new TipoArticuloControl();
            tipoArticuloList = tipoArticuloControl.getTipoArticulos(nombre);
            tipoArticulo = new TipoArticulo();
            String[] titulos = {"ID","Tipo de Articulo"};
            DefaultTableModel modelo = new DefaultTableModel(null,titulos) ;
            Object[] fila = new Object[2];

            for( int i=0; i<tipoArticuloList.size(); i++ ){
                tipoArticulo = (TipoArticulo) tipoArticuloList.get(i);
                //nac =  persona.getFchNacimiento();
                //edad = (int)((HOY.getTime() - nac.getTime())/86400000)/365;
                fila[0] = tipoArticulo.getIdTipoArticulo().toString().trim() ;
                fila[1] = tipoArticulo.getDesTipoArticulo().toString().trim();
                
                modelo.addRow(fila);
            }
            tblTipoArticulos.setModel(modelo);
            Integer ancho = tblTipoArticulos.getSize().width;
            
            TableColumnModel columnModel = tblTipoArticulos.getColumnModel();
            columnModel.getColumn(0).setPreferredWidth(ancho*5/100);
            columnModel.getColumn(1).setPreferredWidth(ancho*95/100);
            
            //limpiar();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSeleccionar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblTipoArticulos;
    private javax.swing.JTextField txtBuscar;
    // End of variables declaration//GEN-END:variables
}
