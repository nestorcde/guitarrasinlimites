/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.Alumno;

import vista.Persona.SelPersonaFrm;
import Entity.Alumno;
import Entity.Persona;
import control.AlumnoControl;
import control.PersonaControl;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author nestor
 */
public class AlumnoFrm extends javax.swing.JDialog {
    public SelPersonaFrm selPersonaFrm;
    AlumnoControl alumnoControl = new AlumnoControl();
    PersonaControl personaControl;
    Alumno alumno = new Alumno();
    public Persona persona = new Persona();
    String modo;
    ListaAlumnosFrm listaAlumnosFrm;
    ImageIcon PERFIL = new ImageIcon(getClass().getResource("/imagenes/ic_account.png"));
    Image imagen;
    Integer id;
    /**
     * Creates new form AlumnoFrm
     */
    public AlumnoFrm() {
        initComponents();
        this.setModal(true);
        this.setLocationRelativeTo(null);
        btnSeleccionar.requestFocus();
    }

//    AlumnoFrm(ListaAlumnosFrm aThis,String mod) {
//         initComponents();
//        this.setLocationRelativeTo(null);
//        btnSeleccionar.requestFocus();
//        modo = mod;
//        listaAlumnosFrm = aThis;
//    }

    AlumnoFrm(ListaAlumnosFrm aThis, String mod, Integer id) {
        initComponents();
        this.setModal(true);
        this.setLocationRelativeTo(null);
        switch (mod){
            case  "VER":
                btnGuardar.setText("SALIR - F4");
                btnSeleccionar.setEnabled(false);
                this.id = id;
                cargarModificar();
                btnGuardar.requestFocus();
                break;
            case "DEL":
                btnGuardar.setText("ELIMINAR - F4");
                btnSeleccionar.setEnabled(false);
                this.id = id;
                cargarModificar();
                btnGuardar.requestFocus();
                break;
            case "INS":
                btnGuardar.setText("GUARDAR - F4");
                listaAlumnosFrm = aThis;
                btnSeleccionar.requestFocus();
                break;
        }
        
        modo = mod;
        
        //JOptionPane.showMessageDialog(null, mod+" "+id);
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtEmail = new javax.swing.JTextField();
        txtCiudad = new javax.swing.JTextField();
        txtDireccion = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        txtCedula = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtFchNac = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        txtIdPersona = new javax.swing.JTextField();
        btnSeleccionar = new javax.swing.JToggleButton();
        lblFoto = new javax.swing.JLabel();
        btnGuardar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DATOS DEL ALUMNO", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 18))); // NOI18N
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("NOMBRES:");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(25, 72, -1, -1));

        jLabel2.setText("CI:");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 101, -1, -1));

        jLabel3.setText("EMAIL:");
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(47, 188, -1, -1));

        jLabel5.setText("DIRECCION:");
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 130, -1, -1));

        jLabel6.setText("CIUDAD:");
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(38, 159, -1, -1));

        jLabel7.setText("ID PERS.:");
        jPanel2.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 37, -1, -1));

        txtNombre.setEditable(false);
        jPanel2.add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(92, 69, 349, -1));

        txtEmail.setEditable(false);
        jPanel2.add(txtEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(92, 185, 349, -1));

        txtCiudad.setEditable(false);
        jPanel2.add(txtCiudad, new org.netbeans.lib.awtextra.AbsoluteConstraints(92, 156, 349, -1));

        txtDireccion.setEditable(false);
        jPanel2.add(txtDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(92, 127, 349, -1));

        jPanel3.setLayout(new javax.swing.BoxLayout(jPanel3, javax.swing.BoxLayout.LINE_AXIS));

        txtCedula.setEditable(false);
        jPanel3.add(txtCedula);

        jLabel4.setText("F. DE NAC.:");
        jPanel3.add(jLabel4);

        txtFchNac.setEditable(false);
        jPanel3.add(txtFchNac);

        jPanel2.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(92, 98, 349, -1));

        jPanel4.setLayout(new javax.swing.BoxLayout(jPanel4, javax.swing.BoxLayout.LINE_AXIS));

        txtIdPersona.setEditable(false);
        jPanel4.add(txtIdPersona);

        btnSeleccionar.setText("SELECCIONAR - F3");
        btnSeleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarActionPerformed(evt);
            }
        });
        btnSeleccionar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnSeleccionarKeyPressed(evt);
            }
        });
        jPanel4.add(btnSeleccionar);

        jPanel2.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(92, 37, 349, -1));

        lblFoto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/ic_account.png"))); // NOI18N

        btnGuardar.setText("GUARDAR - F4");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        btnGuardar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnGuardarKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 462, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(152, 152, 152)
                        .addComponent(lblFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(131, 131, 131)
                        .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSeleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionarActionPerformed
        selPersona();
    }//GEN-LAST:event_btnSeleccionarActionPerformed

    private void btnSeleccionarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnSeleccionarKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_F3){
            selPersona();
            
        }
    }//GEN-LAST:event_btnSeleccionarKeyPressed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        grabarAlumno();
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnGuardarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnGuardarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_F4){
            grabarAlumno();
        }
    }//GEN-LAST:event_btnGuardarKeyPressed

    void selPersona(){
        selPersonaFrm = new SelPersonaFrm(this);
        selPersonaFrm.setVisible(true);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AlumnoFrm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AlumnoFrm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AlumnoFrm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AlumnoFrm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AlumnoFrm().setVisible(true);
            }
        });
    }
    
    void grabarAlumno(){
        if(persona.getIdPersona() != null){
            if(modo == "INS"){
                Alumno alumno = new Alumno();
            }
            Calendar cal = Calendar.getInstance();
            String fecha = null;
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yy");
            if(cal!=null){
                fecha = formato.format(cal.getTime());
            }
            Date fchNac = null;
            try {
                fchNac = new SimpleDateFormat("dd/MM/yy").parse(fecha);
            } catch (ParseException ex) {
                System.out.println("Error de Fecha "+ex.getMessage());
            }
            
            
            try {
                
                switch(modo){
                    case "INS":
                        alumno.setFchRegistro(fchNac);
                        alumno.setIdPersona(persona);
                        alumnoControl.grabar(alumno);
                        listaAlumnosFrm.cargarTabla();
                        dispose();
                        break;
                    case "VER":
                        dispose();
                        break;
                    case "DEL":
                        alumnoControl.eliminar(alumno);
//                        listaAlumnosFrm.tblAlumnos.removeAll();
//                        listaAlumnosFrm.cargarTabla();
                        dispose();
                        break;
                }
            } catch (Exception e) {
                System.out.println("Error al Grabar "+e);
            }
        }else{
            JOptionPane.showMessageDialog(null, "Debe Seleccionar una persona como alumno presionando F3");
        }
    }
    
    void cargarModificar(){
        ImageIcon foto;
            //persona.setIdPersona(id);
            alumno = alumnoControl.getAlumnoes(id).get(0);
            try {
                persona = alumno.getIdPersona();
//                persona = personaControl.getPersonas(id).get(0);
                Calendar cal = Calendar.getInstance();
                String fecha = null;
                cal.setTime(persona.getFchNacimiento());
                SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
                if(cal!=null){
                    fecha = formato.format(cal.getTime());
                }
                txtFchNac.setText(fecha); 
                txtIdPersona.setText(persona.getIdPersona().toString());
                txtCedula.setText(persona.getNroCedula());
                txtNombre.setText(persona.getNomPersona().trim()+' '+persona.getApePersona().trim());
                txtDireccion.setText(persona.getDirPersona());
                txtEmail.setText(persona.getEmail());
                //alumnoFrm.txtTelefono.setText(persona.getTelPersona().trim());
                //cmbSexo.setSelectedItem(persona.getSexo().toString().trim());

                txtCiudad.setText(persona.getIdCiudad().getNomCiudad().trim());
//                Icon ic_perfil = new ImageIcon(PERFIL.getImage().getScaledInstance(lblFoto.getWidth()-25, lblFoto.getHeight()-15, Image.SCALE_DEFAULT));
                Icon ic_perfil2 = new ImageIcon(PERFIL.getImage().getScaledInstance(lblFoto.getWidth()-25, lblFoto.getHeight()-15, Image.SCALE_DEFAULT));
                lblFoto.setIcon(ic_perfil2);
//                lblFoto.setIcon(ic_perfil);
                imagen = null;
                try {
                    BufferedImage img = ImageIO.read(new ByteArrayInputStream(persona.getFotoPersona()));
                    foto = new ImageIcon(img);
                    imagen = foto.getImage();
                    //Image newImagen = img.getScaledInstance(lblImagen.getWidth(), lblImagen.getHeight(), Image.SCALE_SMOOTH);
                    ImageIcon newIcon = new ImageIcon(imagen);
                    lblFoto.setIcon(newIcon);
//                    lblFoto.setIcon(newIcon);

                } catch (Exception e) {
                    System.out.println("Error al Recuperar imagen"+e.getMessage());
                }
            } catch (Exception e) {
                System.out.println("Error al cargar Campos "+e);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnGuardar;
    private javax.swing.JToggleButton btnSeleccionar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    public javax.swing.JLabel lblFoto;
    public javax.swing.JTextField txtCedula;
    public javax.swing.JTextField txtCiudad;
    public javax.swing.JTextField txtDireccion;
    public javax.swing.JTextField txtEmail;
    public javax.swing.JTextField txtFchNac;
    public javax.swing.JTextField txtIdPersona;
    public javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
