/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import Entity.TipoArticulo;
import dato.TipoArticuloDAO;
import java.util.List;

/**
 *
 * @author nestor
 */
public class TipoArticuloControl {
    
    public List<TipoArticulo> getTipoArticulos(){
        return new TipoArticuloDAO().getTipoArticulos();
    }
    
    public List<TipoArticulo> getTipoArticulos(Integer id){
        return new TipoArticuloDAO().getTipoArticulos(id);
    }
    
    public List<TipoArticulo> getTipoArticulos(String nombre){
        return new TipoArticuloDAO().getTipoArticulos(nombre);
    }
    
    public void grabar(TipoArticulo tm){
        new TipoArticuloDAO().grabar(tm);
    }
    
    public void eliminar(Integer id){
        new TipoArticuloDAO().eliminar(id);
    }
    
}
