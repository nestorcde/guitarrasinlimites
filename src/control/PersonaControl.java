/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import Entity.Ciudad;
import Entity.Persona;
import dato.CiudadDAO;
import dato.PersonaDAO;
import java.util.List;

/**
 *
 * @author nestor
 */
public class PersonaControl {
    
    public List<Persona> getPersonas(){
        return new PersonaDAO().getPersonas();
    }
    
    public List<Persona> getPersonas(Integer id){
        return new PersonaDAO().getPersonas(id);
    }
    
    public List<Persona> getPersonasNombre(String nombre, String apellido, String cedula){
        return new PersonaDAO().getPersonasNombre(nombre,apellido,cedula);
    }
    
    public void grabar(Persona p){
        new PersonaDAO().grabar(p);
    }
    
    public void eliminar(Persona p){
        new PersonaDAO().eliminar(p);
    }
    
}
