/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dato.ProfesorDAO;
import java.util.List;
import Entity.Profesor;

/**
 *
 * @author nestor
 */
public class ProfesorControl {
    
    public List<Profesor> getProfesores(){
        return new ProfesorDAO().getProfesores();
    }
    
    public List<Profesor> getProfesores(Integer id){
        return new ProfesorDAO().getProfesores(id);
    }
    
    public List<Profesor> getProfesores(String nombre,String cedula){
        return new ProfesorDAO().getProfesores(nombre,cedula);
    }
    
    public void grabar(Profesor c){
        new ProfesorDAO().grabar(c);
    }
    
    public void eliminar(Profesor c){
        new ProfesorDAO().eliminar(c);
    }
    
}
