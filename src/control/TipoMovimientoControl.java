/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import Entity.TipoMovimiento;
import dato.TipoMovimientoDAO;
import java.util.List;

/**
 *
 * @author nestor
 */
public class TipoMovimientoControl {
    
    public List<TipoMovimiento> getTipoMovimientos(){
        return new TipoMovimientoDAO().getTipoMovimientos();
    }
    
    public List<TipoMovimiento> getTipoMovimientos(Integer id){
        return new TipoMovimientoDAO().getTipoMovimientos(id);
    }
    
    public List<TipoMovimiento> getTipoMovimientos(String nombre){
        return new TipoMovimientoDAO().getTipoMovimientos(nombre);
    }
    
    public void grabar(TipoMovimiento tm){
        new TipoMovimientoDAO().grabar(tm);
    }
    
    public void eliminar(Integer id){
        new TipoMovimientoDAO().eliminar(id);
    }
    
}
