/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import Entity.Curso;
import dato.CursoDAO;
import java.util.List;

/**
 *
 * @author nestor
 */
public class CursoControl {
    
    public List<Curso> getCursos(){
        return new CursoDAO().getCursos();
    }
    
    public List<Curso> getCursos(Integer id){
        return new CursoDAO().getCursos(id);
    }
    
    public List<Curso> getCursos(String nomPersona,String nomCurso){
        return new CursoDAO().getCursos(nomPersona,nomCurso);
    }
    
    public void grabar(Curso c){
        new CursoDAO().grabar(c);
    }
    
    public void eliminar(Curso c){
        new CursoDAO().eliminar(c);
    }
    
}
