/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dato.CuotaDAO;
import java.util.List;
import Entity.Cuota;

/**
 *
 * @author nestor
 */
public class CuotaControl {
    
    public List<Cuota> getCuotas(){
        return new CuotaDAO().getCuotas();
    }
    
    public List<Cuota> getCuotas(int id){
        return new CuotaDAO().getCuotas(id);
    }
        
    public List<Cuota> getCuotas(int id, int sec){
        return new CuotaDAO().getCuotas(id,sec);
    }
    
    public void grabar(Cuota c, String modo){
        new CuotaDAO().grabar(c,modo);
    }
    
    public Integer idCuota(){
        return new CuotaDAO().idCuota();
    }
    
    public void eliminar(Integer idCuota){
        new CuotaDAO().eliminar(idCuota);
    }
    
}
