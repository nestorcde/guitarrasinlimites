/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dato.CiudadDAO;
import java.util.List;
import Entity.Ciudad;

/**
 *
 * @author nestor
 */
public class CiudadControl {
    
    public List<Ciudad> getCiudades(){
        return new CiudadDAO().getCiudades();
    }
    
    public List<Ciudad> getCiudades(Integer id){
        return new CiudadDAO().getCiudades(id);
    }
    
    public void grabar(Ciudad c){
        new CiudadDAO().grabar(c);
    }
    
    public void eliminar(Ciudad c){
        new CiudadDAO().eliminar(c);
    }
    
}
