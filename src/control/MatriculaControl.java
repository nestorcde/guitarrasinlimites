/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import Entity.Alumno;
import Entity.Curso;
import Entity.Matricula;
import dato.MatriculaDAO;
import java.util.List;

/**
 *
 * @author nestor
 */
public class MatriculaControl {
    
    public List<Matricula> getMatriculas(){
        return new MatriculaDAO().getMatriculas();
    }
    
    public List<Matricula> getMatriculas(Integer id){
        return new MatriculaDAO().getMatriculas(id);
    }
    
    public List<Matricula> getMatriculasCurso(Curso curso){
        return new MatriculaDAO().getMatriculasCurso(curso);
    }
    
    public List<Matricula> getMatriculasAlumno(Alumno alumno) {
        return new MatriculaDAO().getMatriculasAlumno(alumno);
    }
    
    public void grabar(Matricula m){
        new MatriculaDAO().grabar(m);
    }
    
    public void eliminar(Matricula m){
        new MatriculaDAO().eliminar(m);
    }

    public List<Matricula> getMatriculas(String curso, String alumno) {
        return new MatriculaDAO().getMatriculas(curso,alumno);
    }
}
