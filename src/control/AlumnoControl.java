/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dato.AlumnoDAO;
import java.util.List;
import Entity.Alumno;

/**
 *
 * @author nestor
 */
public class AlumnoControl {
    
    public List<Alumno> getAlumnoes(){
        return new AlumnoDAO().getAlumnos();
    }
    
    public List<Alumno> getAlumnoes(Integer id){
        return new AlumnoDAO().getAlumnos(id);
    }
    
    public List<Alumno> getAlumnoes(String nombre,String cedula){
        return new AlumnoDAO().getAlumnos(nombre,cedula);
    }
    
    public void grabar(Alumno c){
        new AlumnoDAO().grabar(c);
    }
    
    public void eliminar(Alumno c){
        new AlumnoDAO().eliminar(c);
    }
    
}
