/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import Entity.Articulo;
import dato.ArticuloDAO;
import java.util.List;

/**
 *
 * @author nestor
 */
public class ArticuloControl {
    public List<Articulo> getArticulos(){
        return new ArticuloDAO().getArticulos();
    }
    public List<Articulo> getArticulos(Integer id){
        return new ArticuloDAO().getArticulos(id);
    }
    public List<Articulo> getArticulos(String nombre){
        return new ArticuloDAO().getArticulos(nombre);
    }
    public void grabar(Articulo a){
        new ArticuloDAO().grabar(a);
    }
    public void eliminar(Articulo a){
        new ArticuloDAO().eliminar(a);
    }
}
