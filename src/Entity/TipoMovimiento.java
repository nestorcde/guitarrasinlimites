/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nestor
 */
@Entity
@Table(name = "tipo_movimiento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoMovimiento.findAll", query = "SELECT t FROM TipoMovimiento t Order by t.idTipoMov")
    , @NamedQuery(name = "TipoMovimiento.findByIdTipoMov", query = "SELECT t FROM TipoMovimiento t WHERE t.idTipoMov = :idTipoMov")
    , @NamedQuery(name = "TipoMovimiento.findByDesTipoMov", query = "SELECT t FROM TipoMovimiento t WHERE upper(t.desTipoMov) LIKE CONCAT('%',:desTipoMov,'%') Order by t.idTipoMov")})
public class TipoMovimiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tipo_mov")
    private Integer idTipoMov;
    @Basic(optional = false)
    @Column(name = "des_tipo_mov")
    private String desTipoMov;

    public TipoMovimiento() {
    }

    public TipoMovimiento(Integer idTipoMov) {
        this.idTipoMov = idTipoMov;
    }

    public TipoMovimiento(Integer idTipoMov, String desTipoMov) {
        this.idTipoMov = idTipoMov;
        this.desTipoMov = desTipoMov;
    }

    public Integer getIdTipoMov() {
        return idTipoMov;
    }

    public void setIdTipoMov(Integer idTipoMov) {
        this.idTipoMov = idTipoMov;
    }

    public String getDesTipoMov() {
        return desTipoMov;
    }

    public void setDesTipoMov(String desTipoMov) {
        this.desTipoMov = desTipoMov;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoMov != null ? idTipoMov.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoMovimiento)) {
            return false;
        }
        TipoMovimiento other = (TipoMovimiento) object;
        if ((this.idTipoMov == null && other.idTipoMov != null) || (this.idTipoMov != null && !this.idTipoMov.equals(other.idTipoMov))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.TipoMovimiento[ idTipoMov=" + idTipoMov + " ]";
    }
    
}
