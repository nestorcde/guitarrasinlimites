/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author nestor
 */
@Entity
@Table(name = "curso")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Curso.findAll", query = "SELECT c FROM Curso c")
    , @NamedQuery(name = "Curso.findByIdCurso", query = "SELECT c FROM Curso c WHERE c.idCurso = :idCurso")
    , @NamedQuery(name = "Curso.findByNomCurso", query = "SELECT c FROM Curso c WHERE c.nomCurso = :nomCurso")
    , @NamedQuery(name = "Curso.findByFchFin", query = "SELECT c FROM Curso c WHERE c.fchFin = :fchFin")
    , @NamedQuery(name = "Curso.findByDuracion", query = "SELECT c FROM Curso c WHERE c.duracion = :duracion")
    , @NamedQuery(name = "Curso.findByValMatricula", query = "SELECT c FROM Curso c WHERE c.valMatricula = :valMatricula")
    , @NamedQuery(name = "Curso.findByValContado", query = "SELECT c FROM Curso c WHERE c.valContado = :valContado")
    , @NamedQuery(name = "Curso.findByValParcelado", query = "SELECT c FROM Curso c WHERE c.valParcelado = :valParcelado")
    , @NamedQuery(name = "Curso.findByFchInicio", query = "SELECT c FROM Curso c WHERE c.fchInicio = :fchInicio")})
public class Curso implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCurso")
    private List<Matricula> matriculaList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_curso")
    private Integer idCurso;
    @Basic(optional = false)
    @Column(name = "nom_curso")
    private String nomCurso;
    @Column(name = "fch_fin")
    @Temporal(TemporalType.DATE)
    private Date fchFin;
    @Column(name = "duracion")
    private Integer duracion;
    @Column(name = "val_matricula")
    private Integer valMatricula;
    @Column(name = "val_contado")
    private Integer valContado;
    @Column(name = "val_parcelado")
    private Integer valParcelado;
    @Column(name = "fch_inicio")
    @Temporal(TemporalType.DATE)
    private Date fchInicio;
    @JoinColumn(name = "id_profesor", referencedColumnName = "id_profesor")
    @ManyToOne(optional = false)
    private Profesor idProfesor;

    public Curso() {
    }

    public Curso(Integer idCurso) {
        this.idCurso = idCurso;
    }

    public Curso(Integer idCurso, String nomCurso) {
        this.idCurso = idCurso;
        this.nomCurso = nomCurso;
    }

    public Integer getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(Integer idCurso) {
        this.idCurso = idCurso;
    }

    public String getNomCurso() {
        return nomCurso;
    }

    public void setNomCurso(String nomCurso) {
        this.nomCurso = nomCurso;
    }

    public Date getFchFin() {
        return fchFin;
    }

    public void setFchFin(Date fchFin) {
        this.fchFin = fchFin;
    }

    public Integer getDuracion() {
        return duracion;
    }

    public void setDuracion(Integer duracion) {
        this.duracion = duracion;
    }

    public Integer getValMatricula() {
        return valMatricula;
    }

    public void setValMatricula(Integer valMatricula) {
        this.valMatricula = valMatricula;
    }

    public Integer getValContado() {
        return valContado;
    }

    public void setValContado(Integer valContado) {
        this.valContado = valContado;
    }

    public Integer getValParcelado() {
        return valParcelado;
    }

    public void setValParcelado(Integer valParcelado) {
        this.valParcelado = valParcelado;
    }

    public Date getFchInicio() {
        return fchInicio;
    }

    public void setFchInicio(Date fchInicio) {
        this.fchInicio = fchInicio;
    }

    public Profesor getIdProfesor() {
        return idProfesor;
    }

    public void setIdProfesor(Profesor idProfesor) {
        this.idProfesor = idProfesor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCurso != null ? idCurso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Curso)) {
            return false;
        }
        Curso other = (Curso) object;
        if ((this.idCurso == null && other.idCurso != null) || (this.idCurso != null && !this.idCurso.equals(other.idCurso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Curso[ idCurso=" + idCurso + " ]";
    }

    @XmlTransient
    public List<Matricula> getMatriculaList() {
        return matriculaList;
    }

    public void setMatriculaList(List<Matricula> matriculaList) {
        this.matriculaList = matriculaList;
    }
    
}
