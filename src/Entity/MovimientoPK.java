/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author nestor
 */
@Embeddable
public class MovimientoPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_tipo_mov")
    private int idTipoMov;
    @Basic(optional = false)
    @Column(name = "sec_mov")
    private int secMov;

    public MovimientoPK() {
    }

    public MovimientoPK(int idTipoMov, int secMov) {
        this.idTipoMov = idTipoMov;
        this.secMov = secMov;
    }

    public int getIdTipoMov() {
        return idTipoMov;
    }

    public void setIdTipoMov(int idTipoMov) {
        this.idTipoMov = idTipoMov;
    }

    public int getSecMov() {
        return secMov;
    }

    public void setSecMov(int secMov) {
        this.secMov = secMov;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idTipoMov;
        hash += (int) secMov;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MovimientoPK)) {
            return false;
        }
        MovimientoPK other = (MovimientoPK) object;
        if (this.idTipoMov != other.idTipoMov) {
            return false;
        }
        if (this.secMov != other.secMov) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.MovimientoPK[ idTipoMov=" + idTipoMov + ", secMov=" + secMov + " ]";
    }
    
}
