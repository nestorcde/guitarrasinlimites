/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nestor
 */
@Entity
@Table(name = "matricula")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Matricula.findAll", query = "SELECT m FROM Matricula m ORDER BY m.idMatricula")
    , @NamedQuery(name = "Matricula.findByIdMatricula", query = "SELECT m FROM Matricula m WHERE m.idMatricula = :idMatricula")
    , @NamedQuery(name = "Matricula.findByFchMatricula", query = "SELECT m FROM Matricula m WHERE m.fchMatricula = :fchMatricula")
    , @NamedQuery(name = "Matricula.findByModPago", query = "SELECT m FROM Matricula m WHERE m.modPago = :modPago")
    , @NamedQuery(name = "Matricula.findByIdCurso", query = "SELECT m FROM Matricula m WHERE m.idCurso = :idCurso")
    , @NamedQuery(name = "Matricula.findByIdAlumno", query = "SELECT m FROM Matricula m WHERE m.idAlumno = :idAlumno")
    , @NamedQuery(name = "Matricula.findByParametros", query = "SELECT m FROM Matricula m WHERE (upper(m.idAlumno.idPersona.nomPersona) LIKE CONCAT('%',:nomPersona,'%') OR upper(m.idAlumno.idPersona.apePersona) LIKE CONCAT('%',:nomPersona,'%')) AND upper(m.idCurso.nomCurso) LIKE CONCAT('%',:nomCurso,'%') ORDER BY m.idMatricula")
    , @NamedQuery(name = "Matricula.findByIdCuota", query = "SELECT m FROM Matricula m WHERE m.idCuota = :idCuota")})
public class Matricula implements Serializable {

    @Column(name = "beca")
    private String beca;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_matricula")
    private Integer idMatricula;
    @Basic(optional = false)
    @Column(name = "fch_matricula")
    @Temporal(TemporalType.DATE)
    private Date fchMatricula;
    @Basic(optional = false)
    @Column(name = "mod_pago")
    private Character modPago;
    @Column(name = "id_cuota")
    private Integer idCuota;
    @JoinColumn(name = "id_alumno", referencedColumnName = "id_alumno")
    @ManyToOne(optional = false)
    private Alumno idAlumno;
    @JoinColumn(name = "id_curso", referencedColumnName = "id_curso")
    @ManyToOne(optional = false)
    private Curso idCurso;

    public Matricula() {
    }

    public Matricula(Integer idMatricula) {
        this.idMatricula = idMatricula;
    }

    public Matricula(Integer idMatricula, Date fchMatricula, Character modPago) {
        this.idMatricula = idMatricula;
        this.fchMatricula = fchMatricula;
        this.modPago = modPago;
    }

    public Integer getIdMatricula() {
        return idMatricula;
    }

    public void setIdMatricula(Integer idMatricula) {
        this.idMatricula = idMatricula;
    }

    public Date getFchMatricula() {
        return fchMatricula;
    }

    public void setFchMatricula(Date fchMatricula) {
        this.fchMatricula = fchMatricula;
    }

    public Character getModPago() {
        return modPago;
    }

    public void setModPago(Character modPago) {
        this.modPago = modPago;
    }

    public Integer getIdCuota() {
        return idCuota;
    }

    public void setIdCuota(Integer idCuota) {
        this.idCuota = idCuota;
    }

    public Alumno getIdAlumno() {
        return idAlumno;
    }

    public void setIdAlumno(Alumno idAlumno) {
        this.idAlumno = idAlumno;
    }

    public Curso getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(Curso idCurso) {
        this.idCurso = idCurso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMatricula != null ? idMatricula.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula)) {
            return false;
        }
        Matricula other = (Matricula) object;
        if ((this.idMatricula == null && other.idMatricula != null) || (this.idMatricula != null && !this.idMatricula.equals(other.idMatricula))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Matricula[ idMatricula=" + idMatricula + " ]";
    }

    public String getBeca() {
        return beca;
    }

    public void setBeca(String beca) {
        this.beca = beca;
    }
    
}
