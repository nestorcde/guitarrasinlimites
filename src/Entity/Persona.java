/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author nestor
 */
@Entity
@Table(name = "persona")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Persona.findAll", query = "SELECT p FROM Persona p ORDER BY p.idPersona")
    , @NamedQuery(name = "Persona.findByIdPersona", query = "SELECT p FROM Persona p WHERE p.idPersona = :idPersona")
    , @NamedQuery(name = "Persona.findByNomPersona", query = "SELECT p FROM Persona p WHERE p.nomPersona = :nomPersona")
    , @NamedQuery(name = "Persona.findByApePersona", query = "SELECT p FROM Persona p WHERE p.apePersona = :apePersona")
    , @NamedQuery(name = "Persona.findBySexo", query = "SELECT p FROM Persona p WHERE p.sexo = :sexo")
    , @NamedQuery(name = "Persona.findByFchNacimiento", query = "SELECT p FROM Persona p WHERE p.fchNacimiento = :fchNacimiento")
    , @NamedQuery(name = "Persona.findByTelPersona", query = "SELECT p FROM Persona p WHERE p.telPersona = :telPersona")
    , @NamedQuery(name = "Persona.findByEmail", query = "SELECT p FROM Persona p WHERE p.email = :email")
    , @NamedQuery(name = "Persona.findByDirPersona", query = "SELECT p FROM Persona p WHERE p.dirPersona = :dirPersona")
    , @NamedQuery(name = "Persona.findByNroCedula", query = "SELECT p FROM Persona p WHERE p.nroCedula = :nroCedula")
    , @NamedQuery(name = "Persona.findByParametros", query = "SELECT p FROM Persona p WHERE upper(p.nomPersona) LIKE CONCAT('%',:nomPersona,'%') AND upper(p.apePersona) LIKE CONCAT('%',:apePersona,'%') AND upper(p.nroCedula) LIKE CONCAT('%',:nroCedula,'%')")
    , @NamedQuery(name = "Persona.findAlumno", query = "SELECT p FROM Persona p, Alumno a WHERE a.idPersona.idPersona = p.idPersona")})
public class Persona implements Serializable {

    @Lob
    @Column(name = "foto_persona")
    private byte[] fotoPersona;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPersona")
    private List<Profesor> profesorList;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "idPersona")
    private Alumno alumno;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_persona")
    private Integer idPersona;
    @Basic(optional = false)
    @Column(name = "nom_persona")
    private String nomPersona;
    @Basic(optional = false)
    @Column(name = "ape_persona")
    private String apePersona;
    @Basic(optional = false)
    @Column(name = "sexo")
    private Character sexo;
    @Basic(optional = false)
    @Column(name = "fch_nacimiento")
    @Temporal(TemporalType.DATE)
    private Date fchNacimiento;
    @Basic(optional = false)
    @Column(name = "tel_persona")
    private String telPersona;
    @Column(name = "email")
    private String email;
    @Column(name = "dir_persona")
    private String dirPersona;
    @Column(name = "nro_cedula")
    private String nroCedula;
    @JoinColumn(name = "id_ciudad", referencedColumnName = "id_ciudad")
    @ManyToOne(optional = false)
    private Ciudad idCiudad;

    public Persona() {
    }

    public Persona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Persona(Integer idPersona, String nomPersona, String apePersona, Character sexo, Date fchNacimiento, String telPersona) {
        this.idPersona = idPersona;
        this.nomPersona = nomPersona;
        this.apePersona = apePersona;
        this.sexo = sexo;
        this.fchNacimiento = fchNacimiento;
        this.telPersona = telPersona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNomPersona() {
        return nomPersona;
    }

    public void setNomPersona(String nomPersona) {
        this.nomPersona = nomPersona;
    }

    public String getApePersona() {
        return apePersona;
    }

    public void setApePersona(String apePersona) {
        this.apePersona = apePersona;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public Date getFchNacimiento() {
        return fchNacimiento;
    }

    public void setFchNacimiento(Date fchNacimiento) {
        this.fchNacimiento = fchNacimiento;
    }

    public String getTelPersona() {
        return telPersona;
    }

    public void setTelPersona(String telPersona) {
        this.telPersona = telPersona;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDirPersona() {
        return dirPersona;
    }

    public void setDirPersona(String dirPersona) {
        this.dirPersona = dirPersona;
    }

    public byte[] getFotoPersona() {
        return fotoPersona;
    }

    public void setFotoPersona(byte[] fotoPersona) {
        this.fotoPersona = fotoPersona;
    }

    public String getNroCedula() {
        return nroCedula;
    }

    public void setNroCedula(String nroCedula) {
        this.nroCedula = nroCedula;
    }

    public Ciudad getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Ciudad idCiudad) {
        this.idCiudad = idCiudad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPersona != null ? idPersona.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persona)) {
            return false;
        }
        Persona other = (Persona) object;
        if ((this.idPersona == null && other.idPersona != null) || (this.idPersona != null && !this.idPersona.equals(other.idPersona))) {
            return false;
        }
        return true;
    }

    
//    @Override
//    public String toString() {
//        return "Entity.Persona[ idPersona=" + idPersona + " ]";
//    }

    @Override
    public String toString() {
        return "Persona{" + "idPersona=" + idPersona + ", nomPersona=" + nomPersona + ", apePersona=" + apePersona + ", sexo=" + sexo + ", fchNacimiento=" + fchNacimiento + ", telPersona=" + telPersona + ", email=" + email + ", dirPersona=" + dirPersona + ", fotoPersona=" + fotoPersona + ", nroCedula=" + nroCedula + ", idCiudad=" + idCiudad + '}';
    }

//    public byte[] getFotoPersona() {
//        return fotoPersona;
//    }
//
//    public void setFotoPersona(byte[] fotoPersona) {
//        this.fotoPersona = fotoPersona;
//    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }
//
//    public byte[] getFotoPersona() {
//        return fotoPersona;
//    }
//
//    public void setFotoPersona(byte[] fotoPersona) {
//        this.fotoPersona = fotoPersona;
//    }

    @XmlTransient
    public List<Profesor> getProfesorList() {
        return profesorList;
    }

    public void setProfesorList(List<Profesor> profesorList) {
        this.profesorList = profesorList;
    }
}
