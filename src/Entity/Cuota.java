/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author nestor
 */
@Entity
@Table(name = "cuota")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cuota.findAll", query = "SELECT c FROM Cuota c")
    , @NamedQuery(name = "Cuota.findByIdCuota", query = "SELECT c FROM Cuota c WHERE c.cuotaPK.idCuota = :idCuota")
    , @NamedQuery(name = "Cuota.findByIdSec", query = "SELECT c FROM Cuota c WHERE c.cuotaPK.idCuota = :idCuota AND c.cuotaPK.secCuota = :secCuota")
    , @NamedQuery(name = "Cuota.findBySecCuota", query = "SELECT c FROM Cuota c WHERE c.cuotaPK.secCuota = :secCuota")
    , @NamedQuery(name = "Cuota.findByFchVencimiento", query = "SELECT c FROM Cuota c WHERE c.fchVencimiento = :fchVencimiento")
    , @NamedQuery(name = "Cuota.findByImpCuota", query = "SELECT c FROM Cuota c WHERE c.impCuota = :impCuota")
    , @NamedQuery(name = "Cuota.findByEstCuota", query = "SELECT c FROM Cuota c WHERE c.estCuota = :estCuota")
    , @NamedQuery(name = "Cuota.findByFchPago", query = "SELECT c FROM Cuota c WHERE c.fchPago = :fchPago")
    , @NamedQuery(name = "Cuota.devolverUltSec", query = "SELECT c.cuotaPK.secCuota FROM Cuota c WHERE c.cuotaPK.idCuota = :idCuota ORDER BY c.cuotaPK.secCuota DESC")
    , @NamedQuery(name = "Cuota.findUltIdCuota", query = "SELECT c FROM Cuota c ORDER BY c.cuotaPK.idCuota DESC")})
public class Cuota implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CuotaPK cuotaPK;
    @Column(name = "fch_vencimiento")
    @Temporal(TemporalType.DATE)
    private Date fchVencimiento;
    @Column(name = "imp_cuota")
    private Integer impCuota;
    @Column(name = "est_cuota")
    private Character estCuota;
    @Column(name = "fch_pago")
    @Temporal(TemporalType.DATE)
    private Date fchPago;
    @OneToMany(mappedBy = "cuota")
    private List<Movimiento> movimientoList;

    public Cuota() {
    }

    public Cuota(CuotaPK cuotaPK) {
        this.cuotaPK = cuotaPK;
    }

    public Cuota(int idCuota, int secCuota) {
        this.cuotaPK = new CuotaPK(idCuota, secCuota);
    }

    public CuotaPK getCuotaPK() {
        return cuotaPK;
    }

    public void setCuotaPK(CuotaPK cuotaPK) {
        this.cuotaPK = cuotaPK;
    }

    public Date getFchVencimiento() {
        return fchVencimiento;
    }

    public void setFchVencimiento(Date fchVencimiento) {
        this.fchVencimiento = fchVencimiento;
    }

    public Integer getImpCuota() {
        return impCuota;
    }

    public void setImpCuota(Integer impCuota) {
        this.impCuota = impCuota;
    }

    public Character getEstCuota() {
        return estCuota;
    }

    public void setEstCuota(Character estCuota) {
        this.estCuota = estCuota;
    }

    public Date getFchPago() {
        return fchPago;
    }

    public void setFchPago(Date fchPago) {
        this.fchPago = fchPago;
    }

    @XmlTransient
    public List<Movimiento> getMovimientoList() {
        return movimientoList;
    }

    public void setMovimientoList(List<Movimiento> movimientoList) {
        this.movimientoList = movimientoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cuotaPK != null ? cuotaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cuota)) {
            return false;
        }
        Cuota other = (Cuota) object;
        if ((this.cuotaPK == null && other.cuotaPK != null) || (this.cuotaPK != null && !this.cuotaPK.equals(other.cuotaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Cuota[ cuotaPK=" + cuotaPK + " ]";
    }
    
}
