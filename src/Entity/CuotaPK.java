/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author nestor
 */
@Embeddable
public class CuotaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_cuota")
    private int idCuota;
    @Basic(optional = false)
    @Column(name = "sec_cuota")
    private int secCuota;

    public CuotaPK() {
    }

    public CuotaPK(int idCuota, int secCuota) {
        this.idCuota = idCuota;
        this.secCuota = secCuota;
    }

    public int getIdCuota() {
        return idCuota;
    }

    public void setIdCuota(int idCuota) {
        this.idCuota = idCuota;
    }

    public int getSecCuota() {
        return secCuota;
    }

    public void setSecCuota(int secCuota) {
        this.secCuota = secCuota;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idCuota;
        hash += (int) secCuota;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CuotaPK)) {
            return false;
        }
        CuotaPK other = (CuotaPK) object;
        if (this.idCuota != other.idCuota) {
            return false;
        }
        if (this.secCuota != other.secCuota) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.CuotaPK[ idCuota=" + idCuota + ", secCuota=" + secCuota + " ]";
    }
    
}
