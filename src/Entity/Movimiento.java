/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nestor
 */
@Entity
@Table(name = "movimiento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Movimiento.findAll", query = "SELECT m FROM Movimiento m")
    , @NamedQuery(name = "Movimiento.findByIdTipoMov", query = "SELECT m FROM Movimiento m WHERE m.movimientoPK.idTipoMov = :idTipoMov")
    , @NamedQuery(name = "Movimiento.findBySecMov", query = "SELECT m FROM Movimiento m WHERE m.movimientoPK.secMov = :secMov")
    , @NamedQuery(name = "Movimiento.findByFchMov", query = "SELECT m FROM Movimiento m WHERE m.fchMov = :fchMov")
    , @NamedQuery(name = "Movimiento.findByCantArticulo", query = "SELECT m FROM Movimiento m WHERE m.cantArticulo = :cantArticulo")
    , @NamedQuery(name = "Movimiento.findByImpMov", query = "SELECT m FROM Movimiento m WHERE m.impMov = :impMov")
    , @NamedQuery(name = "Movimiento.findByNumRecibo", query = "SELECT m FROM Movimiento m WHERE m.numRecibo = :numRecibo")})
public class Movimiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MovimientoPK movimientoPK;
    @Column(name = "fch_mov")
    @Temporal(TemporalType.DATE)
    private Date fchMov;
    @Column(name = "cant_articulo")
    private Integer cantArticulo;
    @Column(name = "imp_mov")
    private Integer impMov;
    @Column(name = "num_recibo")
    private Integer numRecibo;
    @JoinColumn(name = "id_articulo", referencedColumnName = "id_articulo")
    @ManyToOne
    private Articulo idArticulo;
    @JoinColumns({
        @JoinColumn(name = "id_cuota", referencedColumnName = "id_cuota")
        , @JoinColumn(name = "sec_cuota", referencedColumnName = "sec_cuota")})
    @ManyToOne
    private Cuota cuota;
    @JoinColumn(name = "id_matricula", referencedColumnName = "id_matricula")
    @ManyToOne
    private Matricula idMatricula;

    public Movimiento() {
    }

    public Movimiento(MovimientoPK movimientoPK) {
        this.movimientoPK = movimientoPK;
    }

    public Movimiento(int idTipoMov, int secMov) {
        this.movimientoPK = new MovimientoPK(idTipoMov, secMov);
    }

    public MovimientoPK getMovimientoPK() {
        return movimientoPK;
    }

    public void setMovimientoPK(MovimientoPK movimientoPK) {
        this.movimientoPK = movimientoPK;
    }

    public Date getFchMov() {
        return fchMov;
    }

    public void setFchMov(Date fchMov) {
        this.fchMov = fchMov;
    }

    public Integer getCantArticulo() {
        return cantArticulo;
    }

    public void setCantArticulo(Integer cantArticulo) {
        this.cantArticulo = cantArticulo;
    }

    public Integer getImpMov() {
        return impMov;
    }

    public void setImpMov(Integer impMov) {
        this.impMov = impMov;
    }

    public Integer getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(Integer numRecibo) {
        this.numRecibo = numRecibo;
    }

    public Articulo getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(Articulo idArticulo) {
        this.idArticulo = idArticulo;
    }

    public Cuota getCuota() {
        return cuota;
    }

    public void setCuota(Cuota cuota) {
        this.cuota = cuota;
    }

    public Matricula getIdMatricula() {
        return idMatricula;
    }

    public void setIdMatricula(Matricula idMatricula) {
        this.idMatricula = idMatricula;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (movimientoPK != null ? movimientoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Movimiento)) {
            return false;
        }
        Movimiento other = (Movimiento) object;
        if ((this.movimientoPK == null && other.movimientoPK != null) || (this.movimientoPK != null && !this.movimientoPK.equals(other.movimientoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Movimiento[ movimientoPK=" + movimientoPK + " ]";
    }
    
}
