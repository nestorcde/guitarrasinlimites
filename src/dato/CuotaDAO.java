/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dato;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import Entity.Cuota;
import Entity.CuotaPK;
import control.CuotaControl;
import javax.swing.JOptionPane;

/**
 *
 * @author nestor
 */
public class CuotaDAO {
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("GuitarraSinLimitesPU");
    private EntityManager em = emf.createEntityManager();
    
    public List<Cuota> getCuotas(){
        TypedQuery query = em.createNamedQuery("Cuota.findAll", Cuota.class);
        List<Cuota> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Integer> getUltSec(int idCuota){
        TypedQuery query = em.createNamedQuery("Cuota.devolverUltSec", Cuota.class).setParameter("idCuota",idCuota);
        List<Integer> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Cuota> getCuotas(int idCuota,int secCuota){
        TypedQuery query = em.createNamedQuery("Cuota.findByIdSec", Cuota.class).setParameter("idCuota", idCuota).setParameter("secCuota", secCuota);
        List<Cuota> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Cuota> getCuotas(int idCuota){
        TypedQuery query = em.createNamedQuery("Cuota.findByIdCuota", Cuota.class).setParameter("idCuota", idCuota);
        List<Cuota> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Cuota> getUltCuota(){
        TypedQuery query = em.createNamedQuery("Cuota.findUltIdCuota", Cuota.class);
        List<Cuota> resultado = query.getResultList();
        return resultado;
    }
    
    public void grabar(Cuota c,String modo){
        try {
            em.getTransaction().begin();
                if(modo.equals("INS")){
                    em.persist(c);
                }else{
                    em.merge(c);
                }
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("error "+e);
        } finally{
            em.close();
        }
    }
    
    public Integer idCuota(){
        List<Cuota> cuotaList = this.getUltCuota();
        Integer idCuota = 1;
        Cuota cuota = new Cuota();
        if(cuotaList.size()>0){
            cuota = cuotaList.get(0);
            idCuota = cuota.getCuotaPK().getIdCuota() + 1;
        }
        return idCuota;
    }
    
    public void eliminar(int idCuota){
        Cuota c = new Cuota();
        CuotaPK cuotaPK = new CuotaPK();
        List<Cuota> cuotaList;
        cuotaList = new CuotaControl().getCuotas(idCuota);
         try {
            for(int i = cuotaList.size(); i >= 1;i--){
                cuotaPK.setIdCuota(idCuota);
                cuotaPK.setSecCuota(i);
               
                    c.setCuotaPK(cuotaPK);
                    em.getTransaction().begin();
                    c = em.getReference(Cuota.class, c.getCuotaPK());
                    em.remove(c);
                    em.getTransaction().commit();
            }
        } catch (Exception e) {
            System.out.println("Error al borrar cuota "+ e.getMessage());
        } finally{
            em.close();
        }
        //JOptionPane.showMessageDialog(null, this.getUltSec(idCuota).get(0));
//        for(int i = 1; i <= this.getUltSec(idCuota).get(0);i++){
//            c = this.getCuotas(idCuota, i).get(0);
//            try {
//                if(idCuota!=0){
//                    em.getTransaction().begin();
//                    c = em.getReference(Cuota.class, idCuota);
//                    em.remove(c);
//                    em.getTransaction().commit();
//                }else{
//                    System.out.println("Id Cuota = null");
//                }
//            } catch (Exception e) {
//                System.out.println("error "+e);
//            } finally{
//                em.close();
//            }
//        }
    }
}
