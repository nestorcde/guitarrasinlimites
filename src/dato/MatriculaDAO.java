/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dato;

import Entity.Alumno;
import Entity.Curso;
import Entity.Matricula;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author nestor
 */
public class MatriculaDAO {
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("GuitarraSinLimitesPU");
    private EntityManager em = emf.createEntityManager();
    
    public List<Matricula> getMatriculas(){
        TypedQuery query = em.createNamedQuery("Matricula.findAll", Matricula.class);
        List<Matricula> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Matricula> getMatriculas(Integer id){
        TypedQuery query = em.createNamedQuery("Matricula.findByIdMatricula", Matricula.class).setParameter("idMatricula", id);
        List<Matricula> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Matricula> getMatriculasCurso(Curso curso){
        TypedQuery query = em.createNamedQuery("Matricula.findByIdCurso", Matricula.class).setParameter("idCurso", curso);
        List<Matricula> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Matricula> getMatriculasAlumno(Alumno alumno){
        TypedQuery query = em.createNamedQuery("Matricula.findByIdAlumno", Matricula.class).setParameter("idAlumno", alumno);
        List<Matricula> resultado = query.getResultList();
        return resultado;
    }
    
    public void grabar(Matricula m){
        try {
            em.getTransaction().begin();
            if(m.getIdCuota()==null){
                em.persist(m);
            }else{
                em.merge(m);
            }
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Error al Grabar Matricula "+e);
        } finally{
            em.close();
        }
    }
    
    public void eliminar(Matricula m){
        if(m!=null){
            try {
                em.getTransaction().begin();
                m = em.getReference(Matricula.class, m.getIdMatricula());
                em.remove(m);
                em.getTransaction().commit();
            } catch (Exception e) {
                System.out.println("Error al eliminar Matricula "+e);
            } finally{
                em.close();
                
            }
        }
    }
    
    public List<Matricula> getMatriculas(String curso,String alumno){
        TypedQuery query = em.createNamedQuery("Matricula.findByParametros", Matricula.class).setParameter("nomCurso", curso).setParameter("nomPersona", alumno);
        List<Matricula> resultado = query.getResultList();
        return resultado;
    }
}
