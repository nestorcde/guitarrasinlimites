/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dato;

import Entity.Articulo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author nestor
 */

public class ArticuloDAO {
    
    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("GuitarraSinLimitesPU");
    private final EntityManager em = emf.createEntityManager();
    
    public List<Articulo> getArticulos(){
        TypedQuery query = em.createNamedQuery("Articulo.findAll", Articulo.class);
        List<Articulo> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Articulo> getArticulos(Integer id){
        TypedQuery query = em.createNamedQuery("Articulo.findByIdArticulo", Articulo.class).setParameter("idArticulo", id);
        List<Articulo> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Articulo> getArticulos(String nombre){
        TypedQuery query = em.createNamedQuery("Articulo.findByDesArticulo", Articulo.class).setParameter("desArticulo", nombre);
        List<Articulo> resultado = query.getResultList();
        return resultado;
    }
    
    public void grabar(Articulo a){
        try {
            em.getTransaction().begin();
            if(a.getIdArticulo()==null){
                em.persist(a);
            }else{
                em.merge(a);
            }
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Error al Grabar Articulo "+e.getLocalizedMessage());
        } finally{
            em.close();
        }
    }
    
    public void eliminar(Articulo a){
        try {
            em.getTransaction().begin();
            if(a.getIdArticulo()!=null){
                a = em.getReference(Articulo.class, a.getIdArticulo());
                em.remove(a);
                em.getTransaction().commit();
            }
        } catch (Exception e) {
            System.out.println("Error al eliminar Articulo "+e.getLocalizedMessage());
        } finally{
            em.close();
        }
    }
    
}
