/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dato;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import Entity.Ciudad;

/**
 *
 * @author nestor
 */
public class CiudadDAO {
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("GuitarraSinLimitesPU");
    private EntityManager em = emf.createEntityManager();
    
    public List<Ciudad> getCiudades(){
        TypedQuery query = em.createNamedQuery("Ciudad.findAll", Ciudad.class);
        List<Ciudad> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Ciudad> getCiudades(Integer id){
        TypedQuery query = em.createNamedQuery("Ciudad.findByIdCiudad", Ciudad.class).setParameter("idCiudad", id);
        List<Ciudad> resultado = query.getResultList();
        return resultado;
    }
    
    public void grabar(Ciudad c){
        try {
            em.getTransaction().begin();
            if(c.getIdCiudad()==null){
                em.persist(c);
            }else{
                em.merge(c);
            }
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("error "+e);
        } finally{
            em.close();
        }
    }
    
    public void eliminar(Ciudad c){
        try {
            if(c.getIdCiudad()!=null){
                em.getTransaction().begin();
                c = em.getReference(Ciudad.class, c.getIdCiudad());
                em.remove(c);
                em.getTransaction().commit();
            }else{
                System.out.println("Id Ciudad = null");
            }
        } catch (Exception e) {
            System.out.println("error "+e);
        } finally{
            em.close();
        }
    }
}
