/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dato;

import Entity.Profesor;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author nestor
 */
public class ProfesorDAO {
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("GuitarraSinLimitesPU");
    private EntityManager em = emf.createEntityManager();
    
    public List<Profesor> getProfesores(){
        TypedQuery query = em.createNamedQuery("Profesor.findAll", Profesor.class);
        List<Profesor> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Profesor> getProfesores(Integer id){
        TypedQuery query = em.createNamedQuery("Profesor.findByIdProfesor", Profesor.class).setParameter("idProfesor", id);
        List<Profesor> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Profesor> getProfesores(String nombre, String cedula){
        TypedQuery query = em.createNamedQuery("Profesor.findByParametros", Profesor.class).setParameter("nomPersona", nombre).setParameter("nroCedula", cedula);
        List<Profesor> resultado = query.getResultList();
        return resultado;
    }
    
    public void grabar(Profesor a){
        try {
            em.getTransaction().begin();
            if(a.getIdProfesor()==null){
                em.persist(a);
            }else{
                em.merge(a);
            }
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Error "+e.getMessage());
        } finally{
            em.close();
        }
    }
    
    public void eliminar(Profesor a){
        try {
            if(a.getIdProfesor()!=null){
                em.getTransaction().begin();
                a = em.getReference(Profesor.class, a.getIdProfesor());
                em.remove(a);
                em.getTransaction().commit();
            }
        } catch (Exception e) {
            System.out.println("Error "+e.getMessage());
        } finally{
            em.close();
        }
    }
}
