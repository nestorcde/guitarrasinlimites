/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dato;

import Entity.Alumno;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author nestor
 */
public class AlumnoDAO {
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("GuitarraSinLimitesPU");
    private EntityManager em = emf.createEntityManager();
    
    public List<Alumno> getAlumnos(){
        TypedQuery query = em.createNamedQuery("Alumno.findAll", Alumno.class);
        List<Alumno> resultado = query.getResultList();
        return resultado;
    }
    
    
    
    public List<Alumno> getAlumnos(Integer id){
        TypedQuery query = em.createNamedQuery("Alumno.findByIdAlumno", Alumno.class).setParameter("idAlumno", id);
        List<Alumno> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Alumno> getAlumnos(String nombre, String cedula){
        TypedQuery query = em.createNamedQuery("Alumno.findByParametros", Alumno.class).setParameter("nomPersona", nombre).setParameter("nroCedula", cedula);
        List<Alumno> resultado = query.getResultList();
        return resultado;
    }
    
    public void grabar(Alumno a){
        try {
            em.getTransaction().begin();
            if(a.getIdAlumno()==null){
                em.persist(a);
            }else{
                em.merge(a);
            }
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Error "+e.getMessage());
        } finally{
            em.close();
        }
    }
    
    public void eliminar(Alumno a){
        try {
            if(a.getIdAlumno()!=null){
                em.getTransaction().begin();
                a = em.getReference(Alumno.class, a.getIdAlumno());
                em.remove(a);
                em.getTransaction().commit();
            }
        } catch (Exception e) {
            System.out.println("Error "+e.getMessage());
        } finally{
            em.close();
        }
    }
}
