/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dato;

import Entity.TipoMovimiento;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author nestor
 */
public class TipoMovimientoDAO {
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("GuitarraSinLimitesPU");
    private EntityManager em = emf.createEntityManager();
    
    public List<TipoMovimiento> getTipoMovimientos(){
        TypedQuery query = em.createNamedQuery("TipoMovimiento.findAll", TipoMovimiento.class);
        List<TipoMovimiento> resultado = query.getResultList();
        return resultado;
    }
    
    public List<TipoMovimiento> getTipoMovimientos(Integer id){
        TypedQuery query = em.createNamedQuery("TipoMovimiento.findByIdTipoMov", TipoMovimiento.class).setParameter("idTipoMov", id);
        List<TipoMovimiento> resultado = query.getResultList();
        return resultado;
    }
    
    public List<TipoMovimiento> getTipoMovimientos(String nombre){
        TypedQuery query = em.createNamedQuery("TipoMovimiento.findByDesTipoMov", TipoMovimiento.class).setParameter("desTipoMov", nombre);
        List<TipoMovimiento> resultado = query.getResultList();
        return resultado;
    }
    
    public void grabar(TipoMovimiento tm){
        try {
            em.getTransaction().begin();
            if(tm.getIdTipoMov()==null){
                em.persist(tm);
            }else{
                em.merge(tm);
            }
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Error al Grabar Tipo de Movimiento: "+e.getLocalizedMessage());
        } finally{
            em.close();
        }
    }
    
    public void eliminar(Integer id){
        if(id!=null){
            try {
                TipoMovimiento tm = new TipoMovimiento();
                tm = this.getTipoMovimientos(id).get(0);
                em.getTransaction().begin();
                tm = em.getReference(TipoMovimiento.class, tm.getIdTipoMov());
                em.remove(tm);
                em.getTransaction().commit();
            } catch (Exception e) {
                System.out.println("Error al eliminar Tipo de Movimiento: "+e.getLocalizedMessage());
            } finally{
                em.close();
            }
        }else{
            System.out.println("El Id de Tipo de Movimiento no puede ser Nulo");
        }
    }
    
}
