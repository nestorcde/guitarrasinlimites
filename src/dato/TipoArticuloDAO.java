/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dato;

import Entity.TipoArticulo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author nestor
 */
public class TipoArticuloDAO {
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("GuitarraSinLimitesPU");
    private EntityManager em = emf.createEntityManager();
    
    public List<TipoArticulo> getTipoArticulos(){
        TypedQuery query = em.createNamedQuery("TipoArticulo.findAll", TipoArticulo.class);
        List<TipoArticulo> resultado = query.getResultList();
        return resultado;
    }
    
    public List<TipoArticulo> getTipoArticulos(Integer id){
        TypedQuery query = em.createNamedQuery("TipoArticulo.findByIdTipoArticulo", TipoArticulo.class).setParameter("idTipoArticulo", id);
        List<TipoArticulo> resultado = query.getResultList();
        return resultado;
    }
    
    public List<TipoArticulo> getTipoArticulos(String nombre){
        TypedQuery query = em.createNamedQuery("TipoArticulo.findByDesTipoArticulo", TipoArticulo.class).setParameter("desTipoArticulo", nombre);
        List<TipoArticulo> resultado = query.getResultList();
        return resultado;
    }
    
    public void grabar(TipoArticulo ta){
        try {
            em.getTransaction().begin();
            if(ta.getIdTipoArticulo()==null){
                em.persist(ta);
            }else{
                em.merge(ta);
            }
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Error al Grabar Tipo de Articulo: "+e.getLocalizedMessage());
        } finally{
            em.close();
        }
    }
    
    public void eliminar(Integer id){
        if(id!=null){
            try {
                TipoArticulo ta = new TipoArticulo();
                ta = this.getTipoArticulos(id).get(0);
                em.getTransaction().begin();
                ta = em.getReference(TipoArticulo.class, ta.getIdTipoArticulo());
                em.remove(ta);
                em.getTransaction().commit();
            } catch (Exception e) {
                System.out.println("Error al eliminar Tipo de Articulo: "+e.getLocalizedMessage());
            } finally{
                em.close();
            }
        }else{
            System.out.println("El Id de Tipo de Articulo no puede ser Nulo");
        }
    }
    
}
