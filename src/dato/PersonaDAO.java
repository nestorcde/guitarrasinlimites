/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dato;

import Entity.Persona;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author nestor
 */
public class PersonaDAO {
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("GuitarraSinLimitesPU");
    private EntityManager em = emf.createEntityManager();
    
    public List<Persona> getPersonas() {
        TypedQuery query = em.createNamedQuery("Persona.findAll", Persona.class);
        List<Persona> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Persona> getPersonas(Integer id) {
        TypedQuery query = em.createNamedQuery("Persona.findByIdPersona", Persona.class).setParameter("idPersona", id);
        List<Persona> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Persona> getPersonasNombre(String nombre,String apellido,String cedula){
        TypedQuery query = em.createNamedQuery("Persona.findByParametros", 
                Persona.class).setParameter("nomPersona", nombre)
                .setParameter("apePersona", apellido).setParameter("nroCedula", 
                        cedula);
        List<Persona> resultado = query.getResultList();
        return resultado;
    }
    
    public void grabar(Persona p){
        try {
            em.getTransaction().begin();
            if(p.getIdPersona()==null){
                em.persist(p);
            }else{
                em.merge(p);
            }
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("error "+e.getMessage());
        } finally{
            em.close();
        }
    }
    
    public void eliminar(Persona p){
        try {
            if(p.getIdPersona()!=null){
                em.getTransaction().begin();
                p = em.getReference(Persona.class, p.getIdPersona());
                em.remove(p);
                em.getTransaction().commit();
            }else{
                System.out.println("IdPersona == null");
            }
        } catch (Exception e) {
            System.out.println("error "+e.getMessage());
        } finally{
            em.close();
        }
    }
    
}
