/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dato;

import Entity.Curso;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author nestor
 */
public class CursoDAO {
    
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("GuitarraSinLimitesPU");
    private EntityManager em = emf.createEntityManager();
    
    public List<Curso> getCursos(){
        TypedQuery query = em.createNamedQuery("Curso.findAll", Curso.class);
        List<Curso> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Curso> getCursos(Integer id){
        TypedQuery query = em.createNamedQuery("Curso.findByIdCurso", Curso.class).setParameter("idCurso", id);
        List<Curso> resultado = query.getResultList();
        return resultado;
    }
    
    public List<Curso> getCursos(String nomProf, String nomCurso){
        TypedQuery query = em.createNamedQuery("Curso.findByParametros", Curso.class).setParameter("nomPersona", nomProf).setParameter("nomCurso", nomCurso);
        List<Curso> resultado = query.getResultList();
        return resultado;
    }
    
    public void grabar(Curso c){
        try {
            em.getTransaction().begin();
            if(c.getIdCurso()==null){
                em.persist(c);
            }else{
                em.merge(c);
            }
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Error al Grabar "+e);
        } finally{
            em.close();
        }
    }
    
    public void eliminar(Curso c){
        if(c.getIdCurso()!=null){
            try {
                em.getTransaction().begin();
                c = em.getReference(Curso.class, c.getIdCurso());
                em.remove(c);
                em.getTransaction().commit();
            } catch (Exception e) {
                System.out.println("Error al eliminar "+e);
            } finally{
                em.close();
            }
        }
    }
}
